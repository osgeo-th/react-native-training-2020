# react-native-training-2020

### Bonus

Application GIS Boilerplate for starter Survey App on Android && IOS by MapboxSDK in Folder [ApplicationGisBoilerplate](ApplicationGisBoilerplate)

### Feature

- Render background by Thailand Basemap
- CRUD Feature "Point, Line, Polygon" in format Geojson
- Connect openapi OSGEO-TH for storage data
- Access Geolocation for show curent location
- Access Camera & Image gallary for Geojson
- Control layer (show & hide)
