import React, {useState} from 'react';
import {TouchableOpacity, StyleSheet, Text, View} from 'react-native';
import ModalSave from './ModalSave';
import * as turf from '@turf/turf';
const CreateBtn = ({
  type,
  statePoint,
  setStatePoint,
  point,
  settype,
  jsonarray,
  setJsonaaray,
}) => {
  const [open, setOpen] = useState(false);

  const onCreatePoint = (input) => {
    let obj = {};
    for (var i = 0; i < input.length; i++) {
      obj[input[i].name] = input[i].value;
    }
    point[0].properties = obj;

    let pointjson = [...statePoint];
    pointjson.push(point[0]);

    setStatePoint(pointjson);
    settype(null);
    setOpen(false);
  };

  const addLine = () => {
    const join = jsonarray.line.concat([point[0].geometry.coordinates]);

    if (join.length !== 0) {
      setJsonaaray({...jsonarray, line: join});
    } else {
      const prepoint = turf.point(point[0].geometry.coordinates);
      setJsonaaray({...jsonarray, point: [prepoint]});
      // setJsonaaray({...jsonarray, point: [point[0].geometry.coordinates]});
    }
  };

  const onCreateLine = (input) => {
    let obj = {};
    for (var i = 0; i < input.length; i++) {
      obj[input[i].name] = input[i].value;
    }

    const line = turf.lineString(jsonarray.line);
    line.properties = obj;

    let pointjson = [...statePoint];
    pointjson.push(line);

    setStatePoint(pointjson);
    settype(null);
    setOpen(false);

    setJsonaaray({...jsonarray, point: [], line: [], polygon: []});
  };
  return (
    <>
      {type === null ? null : type === 'point' ? (
        <TouchableOpacity
          style={styles.save}
          activeOpacity={0.8}
          onPress={() => {
            setOpen(true);
          }}>
          <Text style={styles.savet}>Save</Text>
        </TouchableOpacity>
      ) : (
        <View style={styles.compoadd}>
          <TouchableOpacity style={styles.add} activeOpacity={0.8}>
            <Text
              style={styles.savet}
              onPress={() => {
                addLine();
              }}>
              Add
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.addsave} activeOpacity={0.8}>
            <Text
              style={styles.savet}
              onPress={() => {
                setOpen(true);
              }}>
              Save
            </Text>
          </TouchableOpacity>
        </View>
      )}
      <ModalSave
        open={open}
        setOpen={setOpen}
        onComplete={
          type === 'point'
            ? onCreatePoint
            : type === 'line'
            ? onCreateLine
            : () => {}
        }
      />
    </>
  );
};

export default CreateBtn;

const styles = StyleSheet.create({
  save: {
    borderRadius: 10,
    position: 'absolute',
    bottom: 23,
    zIndex: 5,
    width: '70%',
    backgroundColor: '#4bb543',
    padding: 15,
    left: 20,
  },
  savet: {color: '#ffffff', textAlign: 'center', fontSize: 16},
  compoadd: {
    position: 'absolute',
    bottom: 23,
    zIndex: 5,
    left: 20,
    display: 'flex',
    flexDirection: 'row',
    width: '70%',
    justifyContent: 'space-between',
  },
  add: {
    borderRadius: 10,
    backgroundColor: '#557CB5',
    padding: 15,
    width: '47%',
  },
  addsave: {
    borderRadius: 10,
    backgroundColor: '#4bb543',
    padding: 15,
    width: '47%',
  },
});
