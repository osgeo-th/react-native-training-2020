import React from 'react';
import MapboxGL from '@react-native-mapbox-gl/maps';

const CreateLine = ({linecoor, pointcoor}) => {
  const prevLine = {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {},
        geometry: {
          type: 'LineString',
          coordinates: linecoor,
        },
      },
    ],
  };

  const prevPoint = {
    type: 'FeatureCollection',
    features: pointcoor,
  };

  return (
    <>
      <MapboxGL.ShapeSource id="preview_line_source" shape={prevLine}>
        <MapboxGL.CircleLayer
          id="preview_line_point"
          style={{circleRadius: 5, circleColor: '#ff0000'}}
        />
        <MapboxGL.LineLayer
          id="preview_line"
          style={{lineColor: '#ff0000', lineWidth: 5}}
        />
      </MapboxGL.ShapeSource>

      <MapboxGL.ShapeSource id="preview_point_source" shape={prevPoint}>
        <MapboxGL.CircleLayer
          id="preview_point"
          style={{circleRadius: 5, circleColor: '#ff0000'}}
        />
      </MapboxGL.ShapeSource>
    </>
  );
};

export default CreateLine;
