import React, {useState} from 'react';
import {
  View,
  Text,
  Modal,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import {
  Header,
  Left,
  Button,
  Icon,
  Body,
  Right,
  Form,
  Item,
  Label,
  Input,
} from 'native-base';
import {useRoute} from '@react-navigation/native';
import ImagePicker from 'react-native-image-picker';
import {uploadFile} from '../api/Other';

const ModalSave = ({open, setOpen, onComplete}) => {
  const {params} = useRoute();
  console.log(params.data);
  const [name, setName] = useState('');
  const [image, setImage] = useState([]);

  const [data, setData] = useState(
    params.data.map((rs) => {
      return {
        name: rs.name,
        value: '',
        type: rs.data_type,
      };
    }),
  );

  const onChange = (i, e) => {
    let item = [...data];
    item[i].value = e;
    setData(item);
  };

  const options = {
    title: 'Select Avatar',
    customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  const selectImage = () => {
    ImagePicker.showImagePicker(options, async (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // const source = {uri: response.uri};

        const source = {uri: response.uri, src: response};
        const ImageData = [...image];
        ImageData.push(source);
        setImage(ImageData);

        const upload = await uploadFile(response);

        console.log(upload);
        setImage(source);
      }
    });
  };

  return (
    <View>
      <Modal animationType="slide" visible={open}>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => {
                setOpen(false);
              }}>
              <Icon name="arrowleft" type="AntDesign" size={20} />
            </Button>
          </Left>
          <Body>
            <Text style={styles.clcname}>Save</Text>
          </Body>
          <Right>
            <TouchableOpacity
              onPress={() => {
                let inputName = {name: 'name', type: 'string', value: name};
                data.push(inputName);

                onComplete(data);

                setName('');
                setData(
                  params.data.map((rs) => {
                    return {
                      name: rs.name,
                      value: '',
                      type: rs.data_type,
                    };
                  }),
                );
                // onComplete();
              }}>
              <Icon
                style={styles.clcname}
                name="save"
                type="AntDesign"
                size={20}
              />
            </TouchableOpacity>
          </Right>
        </Header>
        <View>
          <Form>
            <Item floatingLabel>
              <Label>Name</Label>
              <Input
                value={name}
                onChangeText={(e) => {
                  setName(e);
                }}
              />
            </Item>
            {data.map((rs, index) => {
              return (
                <Item floatingLabel key={`${rs.name}_${index}`}>
                  <Label>{rs.name}</Label>
                  <Input
                    value={rs.value}
                    keyboardType={rs.type === 'int' ? 'numeric' : 'default'}
                    onChangeText={(e) => {
                      onChange(index, e);
                    }}
                  />
                </Item>
              );
            })}
          </Form>

          <Image source={image} style={{width: 150, height: 150}} />
        </View>

        <View style={styles.position}>
          <Button
            style={{textAlign: 'center'}}
            onPress={() => {
              selectImage();
            }}>
            <Icon type="FontAwesome" name="camera" />
          </Button>
        </View>
      </Modal>
    </View>
  );
};

export default ModalSave;
const styles = StyleSheet.create({
  clcname: {color: '#ffffff'},
  position: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    zIndex: 5,
  },
});
