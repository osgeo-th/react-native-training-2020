export const uploadFile = (files) => {
  const requestURL = 'https://ticapi.mots.go.th/v1.0/uploads';
  const requestHeader = {
    'Content-Type': 'multipart/form-data',
  };

  let body = new FormData();
  body.append('file', {
    uri: files.uri,
    name: 'photo.png',
    filename: 'imageName.png',
    type: 'image/png',
  });
  body.append('Content-Type', 'image/png');

  return fetch(requestURL, {
    method: 'POST',
    headers: requestHeader,
    body: body,
  })
    .then((response) => response.json())
    .then((response) => {
      return response;
    })
    .catch((error) => {
      return error;
    });
};
