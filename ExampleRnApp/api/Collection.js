import Config from '../Config';

export const getCollectionlist = () => {
  const reqestURL = `${Config.url}/${Config.version}/collections`;
  const reqestHeader = {
    'Content-Type': 'application/json',
    'API-Key': Config.apiKey,
  };
  return fetch(reqestURL, {
    method: 'GET',
    headers: reqestHeader,
  })
    .then((response) => response.json())
    .then((response) => {
      return response;
    })
    .catch((error) => {
      return error;
    });
};

export const addCollection = (data) => {
  const reqestURL = 'https://openapi.osgeo.in.th/v1.1/collections';
  const reqestHeader = {
    'Content-Type': 'application/json',
    'API-Key': Config.apiKey,
  };
  return fetch(reqestURL, {
    method: 'POST',
    headers: reqestHeader,
    body: JSON.stringify(data),
  })
    .then((response) => response.json())
    .then((response) => {
      return response;
    })
    .catch((error) => {
      return error;
    });
};

export const deleteCollection = (id) => {
  const reqestURL = `https://openapi.osgeo.in.th/v1.1/collections/${id}`;
  const reqestHeader = {
    'Content-Type': 'application/json',
    'API-Key': Config.apiKey,
  };

  return fetch(reqestURL, {
    method: 'DELETE',
    headers: reqestHeader,
  })
    .then((response) => response)
    .then((response) => {
      if (response.status < 300) {
        return {status: {code: response.status}};
      } else {
        return response.json();
      }
    })
    .catch((error) => {
      return error;
    });
};
