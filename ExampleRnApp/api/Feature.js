import Config from '../Config';

export const addFeature = (c_id, geojson) => {
  const reqestURL = `${Config.url}/${Config.version}/collections/${c_id}/items`;
  const reqestHeader = {
    'Content-Type': 'application/json',
    'API-Key': Config.apiKey,
  };
  return fetch(reqestURL, {
    method: 'POST',
    headers: reqestHeader,
    body: JSON.stringify(geojson),
  })
    .then((response) => response.json())
    .then((response) => {
      return response;
    })
    .catch((error) => {
      return error;
    });
};

export const getFeaturesList = (c_id) => {
  const reqestURL = `${Config.url}/${Config.version}/collections/${c_id}/items`;
  const reqestHeader = {
    'Content-Type': 'application/json',
    'API-Key': Config.apiKey,
  };
  return fetch(reqestURL, {
    method: 'GET',
    headers: reqestHeader,
  })
    .then((response) => response.json())
    .then((response) => {
      return response;
    })
    .catch((error) => {
      return error;
    });
};
