import React, {createContext, useState} from 'react';

export const ProjectContext = createContext();
export const ProjectContextProvider = (props) => {
  const [data, setData] = useState([]);
  return (
    <ProjectContext.Provider
      value={{
        data: data,
        setData: setData,
      }}>
      {props.children}
    </ProjectContext.Provider>
  );
};
