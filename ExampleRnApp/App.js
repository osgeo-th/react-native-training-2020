import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import HomeScreen from './screens/HomeScreen';
import AboutScreen from './screens/AboutScreen';
import MapScreen from './screens/MapScreen';
import DrawerContent from './screens/DrawerContent';
import ProjectScreen from './screens/ProjectScreen';
import {ProjectContextProvider} from './context/ProjectContext';

const Drawer = createDrawerNavigator();

const App = () => {
  return (
    <ProjectContextProvider>
      <NavigationContainer>
        <Drawer.Navigator
          initialRouteName="About"
          drawerContent={(props) => <DrawerContent {...props} />}>
          <Drawer.Screen
            name="Home"
            component={HomeScreen}
            options={{title: 'Home'}}
          />
          <Drawer.Screen name="Collection" component={ProjectScreen} />
          <Drawer.Screen name="About" component={AboutScreen} />
          <Drawer.Screen name="Map" component={MapScreen} />
        </Drawer.Navigator>
      </NavigationContainer>
    </ProjectContextProvider>
  );
};

export default App;
console.disableYellowBox = true;
