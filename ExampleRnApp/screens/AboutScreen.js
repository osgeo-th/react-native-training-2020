import React, {useState, useEffect, useContext, useCallback} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import {
  Container,
  Header,
  Body,
  Title,
  Text,
  List,
  ListItem,
  Button,
  Left,
  Right,
  Icon,
} from 'native-base';
import {ScrollView, StyleSheet, View} from 'react-native';
import {ProjectContext} from '../context/ProjectContext';
import {getCollectionlist, deleteCollection} from '../api/Collection';
const AboutScreen = () => {
  const {navigate} = useNavigation();
  const route = useRoute();

  const projectcontext = useContext(ProjectContext);
  const [mount, setmount] = useState(false);
  const ProjectData = projectcontext.data;
  const ProjectsetData = projectcontext.setData;

  const getData = useCallback(async () => {
    console.log('get');
    const callApi = await getCollectionlist();
    ProjectsetData(callApi.data);
    setmount(true);
  }, [ProjectsetData]);

  useEffect(() => {
    if (!mount) {
      getData();
    }
  }, [mount, getData]);

  useEffect(() => {
    if (route.params) {
      if (route.params.addData) {
        getData();
      }
    }
  }, [route.params, getData]);

  const Adddata = async () => {
    navigate('Collection');
  };

  const DeleteData = (id) => {
    deleteCollection(id).then((rs) => {
      if (rs.status.code < 300) {
        getData();
      } else {
        alert(`${rs.status.message}`);
      }
    });
  };
  return (
    <Container>
      <Header>
        <Body>
          <Title>Project</Title>
        </Body>
      </Header>

      <ScrollView style={{marginBottom: 80}}>
        <View>
          <List>
            {ProjectData.map((rs, index) => {
              return (
                <ListItem key={`${rs.name} - ${index}`} style={styles.itemlist}>
                  <Left>
                    <Text style={styles.textProject}>{rs.name}</Text>
                  </Left>
                  <Right style={{flexDirection: 'row'}}>
                    <Button
                      onPress={() => {
                        navigate('Map', {
                          name: rs.name,
                          id: rs._id,
                          data: rs.custom_fields,
                        });
                      }}
                      style={styles.actionbtn}
                      rounded
                      info>
                      <Icon
                        style={styles.icon}
                        type="FontAwesome"
                        name="edit"
                      />
                    </Button>
                    <Button
                      onPress={() => {
                        DeleteData(rs._id);
                      }}
                      style={styles.actionbtn}
                      rounded
                      danger>
                      <Icon
                        style={styles.icon}
                        type="FontAwesome"
                        name="trash"
                      />
                    </Button>
                  </Right>
                </ListItem>
              );
            })}
          </List>
        </View>
      </ScrollView>

      <Button
        style={styles.addbtn}
        onPress={() => {
          Adddata();
        }}
        rounded
        success>
        <Text>Add</Text>
      </Button>
    </Container>
  );
};
const styles = StyleSheet.create({
  itemlist: {backgroundColor: '#d6d6d6', borderRadius: 20, margin: 10},
  textProject: {paddingLeft: 10},
  addbtn: {position: 'absolute', right: 20, bottom: 20, zIndex: 10},
  icon: {fontSize: 15, textAlign: 'center'},
  actionbtn: {width: 45, justifyContent: 'center'},
});
export default AboutScreen;
