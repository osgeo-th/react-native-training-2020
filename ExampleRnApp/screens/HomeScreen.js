import React, {useContext} from 'react';
import {View, StyleSheet} from 'react-native';
import {
  Container,
  Header,
  Text,
  Body,
  Title,
  List,
  ListItem,
  Left,
  Right,
  Button,
  Icon,
} from 'native-base';
import {useNavigation} from '@react-navigation/native';
import {ProjectContext} from '../context/ProjectContext';

const arrayProjects = [
  {
    name: 'Colection 1',
    custom_fields: [],
  },
  {
    name: 'Colection 2',
    custom_fields: [],
  },
  {
    name: 'Colection 3',
    custom_fields: [],
  },
];

const HomeScreen = () => {
  const projectsContext = useContext(ProjectContext);
  const projects = projectsContext.data;
  const setprojects = projectsContext.setData;
  const {navigate} = useNavigation();

  const deleteRow = (name) => () => {
    setprojects(projects.filter((proj) => proj.name !== name));
  };

  const editProject = (proj) => () => {
    navigate('Map', proj);
  };

  return (
    <Container>
      <Header>
        <Body>
          <Title>Projects</Title>
        </Body>
        <Right>
          <Button
            transparent
            onPress={() => {
              navigate('Project');
            }}>
            <Icon
              type="FontAwesome"
              style={{color: '#fff', fontSize: 20, textAlign: 'center'}}
              name="plus"
            />
          </Button>
        </Right>
      </Header>
      <View>
        <List>
          {projects.map((proj, index) => (
            <ListItem button key={`${proj.name}-${index}`}>
              <Left>
                <Text>{proj.name}</Text>
              </Left>
              <Right style={styles.rightAction}>
                <Button
                  style={styles.actionBtn}
                  rounded
                  onPress={editProject(proj)}>
                  <Icon
                    name="pencil"
                    type="FontAwesome"
                    style={{color: '#fff', fontSize: 20, textAlign: 'center'}}
                  />
                </Button>
                <Button
                  style={styles.actionBtn}
                  rounded
                  danger
                  onPress={deleteRow(proj.name)}>
                  <Icon
                    type="FontAwesome"
                    style={{color: '#fff', fontSize: 20, textAlign: 'center'}}
                    name="trash-o"
                  />
                </Button>
              </Right>
            </ListItem>
          ))}
        </List>
      </View>
    </Container>
  );
};

const styles = StyleSheet.create({
  rightAction: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  actionBtn: {width: 45, marginLeft: 8, justifyContent: 'center'},
});

export default HomeScreen;
