import React, {useState, Fragment, useContext} from 'react';
import {
  Container,
  Header,
  Item,
  Input,
  Body,
  Title,
  Text,
  Picker,
  Form,
  Button,
  Icon,
  Right,
} from 'native-base';
import {useNavigation} from '@react-navigation/native';
import {View, StyleSheet} from 'react-native';
import {ProjectContext} from '../context/ProjectContext';
import {addCollection} from '../api/Collection';
const styles = StyleSheet.create({
  actionBtn: {width: 45, marginLeft: 8, justifyContent: 'center'},
});
const ProjectScreen = (props) => {
  const {navigate} = useNavigation();
  const projectsContext = useContext(ProjectContext);
  const projects = projectsContext.data;
  const setprojects = projectsContext.setData;
  const [data, setdata] = useState({
    name: '',
    custom_fields: [
      {
        name: '',
        alias: '',
        data_type: 'string',
      },
    ],
  });

  const EditNameData = (value, index) => {
    let newdata = {...data};
    newdata.custom_fields[index].name = value;
    setdata(newdata);
  };

  const EditAliasData = (value, index) => {
    let newdata = {...data};
    newdata.custom_fields[index].alias = value;
    setdata(newdata);
  };

  const EditTypeData = (value, index) => {
    let newdata = {...data};
    newdata.custom_fields[index].data_type = value;

    setdata(newdata);
  };

  const AddProject = () => {
    addCollection(data).then((rs) => {
      if (rs.status.code === 201) {
        // setprojects([...projects, ...rs.data]);
        navigate('About', {addData: true});
      } else {
        alert(`error ${rs.status.message}`);
      }
    });
  };
  return (
    <Container>
      <Header>
        <Body>
          <Title>Create</Title>
        </Body>
      </Header>
      <View style={{padding: 20}}>
        {data && (
          <Fragment>
            <Item rounded>
              <Input
                value={data.name}
                onChangeText={(text) => {
                  setdata({...data, name: text});
                }}
                placeholder="Collection Name"
              />
            </Item>
            <Text style={{marginVertical: 10}}>Attribute</Text>
            {data.custom_fields.map((cs, index) => {
              return (
                <View key={`att-${index}`}>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    <Item rounded style={{width: '50%'}}>
                      <Input
                        value={cs.name}
                        onChangeText={(text) => {
                          EditNameData(text, index, data);
                        }}
                        placeholder="Name"
                      />
                    </Item>
                    <Item rounded style={{width: '50%'}}>
                      <Input
                        value={cs.alias}
                        onChangeText={(text) => {
                          EditAliasData(text, index, data);
                        }}
                        placeholder="Alias"
                      />
                    </Item>
                  </View>
                  <View>
                    <Text style={{marginVertical: 10}}>Data type</Text>
                    <Form>
                      <Item picker style={{marginBottom: 10}}>
                        <Picker
                          mode="dropdown"
                          placeholderIconColor="#007aff"
                          selectedValue={cs.data_type}
                          onValueChange={(value) => {
                            EditTypeData(value, index, data);
                          }}>
                          <Picker.Item label="string" value="string" />
                          <Picker.Item label="int" value="int" />
                        </Picker>
                      </Item>
                    </Form>
                  </View>
                </View>
              );
            })}
            <View
              style={{
                marginTop: 10,
                display: 'flex',
                justifyContent: 'center',
                flexDirection: 'row',
              }}>
              {/* <Button
                style={styles.actionBtn}
                rounded
                onPress={() => {
                  AddcustomField();
                }}>
                <Icon
                  type="FontAwesome"
                  style={{color: '#fff', fontSize: 20, textAlign: 'center'}}
                  name="plus"
                />
              </Button> */}
            </View>
          </Fragment>
        )}
      </View>
      <Button
        style={{
          position: 'absolute',

          right: 15,
          bottom: 15,
        }}
        rounded
        success
        onPress={() => {
          AddProject();
        }}>
        <Text style={{marginVertical: 10}}>Add</Text>
      </Button>
    </Container>
  );
};

export default ProjectScreen;
