import React, {useState, useRef, useEffect, useCallback} from 'react';
import {StyleSheet, View} from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';
import {
  Container,
  Header,
  Left,
  Icon,
  Body,
  Title,
  Button,
  Fab,
  Right,
} from 'native-base';
import {useRoute, useNavigation} from '@react-navigation/native';
import CreateBtn from '../component/CreateBtn';
import MarkerImage from '../images/marker_plus.png';
import * as turf from '@turf/turf';
import CreateLine from '../component/CreateLine';
import {addFeature, getFeaturesList} from '../api/Feature';
import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';

MapboxGL.setAccessToken('ABC');
const styles = StyleSheet.create({
  page: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  container: {
    height: '100%',
    width: '100%',
    backgroundColor: 'transparent',
    flex: 1,
  },
  map: {
    flex: 1,
  },
});
const MapScreen = () => {
  const {params} = useRoute();
  const {navigate} = useNavigation();
  const [fetch, setFetch] = useState(false);
  const [state, setState] = useState(null);
  const [active, setActive] = useState(false);
  const [type, setType] = useState(null);
  const mapRef = useRef(null);

  const [addpoint, setAddPoint] = useState([]);
  const [stateJson, setStateJson] = useState([]);

  const [jsonarray, setJsonaaray] = useState({
    point: [],
    line: [],
    polygon: [],
  });

  const geoState = {
    type: 'FeatureCollection',
    features: stateJson,
  };

  const geoJson = {
    type: 'FeatureCollection',
    features: addpoint,
  };

  const onRegionChange = async () => {
    if (type !== null) {
      const center = await mapRef.current.getCenter();
      const point_feature = turf.point(center);
      setAddPoint([point_feature]);
    } else {
    }
  };

  const onCreateFeature = async () => {
    const add = await addFeature(params.id, geoState);
    if (add && add.status === undefined) {
      setFetch(false);
    }
  };

  const onGetFeatures = useCallback(async () => {
    const get = await getFeaturesList(params.id);
    if (get && get.status === undefined) {
      setStateJson(get.features);
    }
  }, []);

  useEffect(() => {
    if (!fetch) {
      onGetFeatures();
      setFetch(true);
    }
  }, [fetch, onGetFeatures]);

  const getMyLocation = () => {
    LocationServicesDialogBox.checkLocationServicesIsEnabled({
      message:
        "<h2>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/><a href='#'>Learn more</a>",
      ok: 'YES',
      cancel: 'NO',
      enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
      showDialog: true, // false => Opens the Location access page directly
      openLocationServices: true, // false => Directly catch method is called if location services are turned off
      preventOutSideTouch: false, //true => To prevent the location services popup from closing when it is clicked outside
      preventBackClick: false, //true => To prevent the location services popup from closing when it is clicked back button
      providerListener: true, // true ==> Trigger "locationProviderStatusChange" listener when the location state changes
    })
      .then(function (success) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            alert(position);
          },
          (error) => console.log(error),
          {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000},
        );
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  return (
    <Container>
      <Header>
        <Left>
          <Button
            transparent
            onPress={() => {
              navigate('About');
            }}>
            <Icon
              type="FontAwesome"
              name="arrow-left"
              style={{color: '#faf7f1'}}
            />
          </Button>
        </Left>
        <Body>
          <Title>{params.name}</Title>
        </Body>
        <Right>
          <Button
            transparent
            onPress={() => {
              onCreateFeature();
            }}>
            <Icon
              type="AntDesign"
              name="cloudupload"
              style={{color: '#faf7f1'}}
            />
          </Button>
        </Right>
      </Header>
      <View style={styles.container}>
        <CreateBtn
          type={type}
          settype={setType}
          point={addpoint}
          statePoint={stateJson}
          setStatePoint={setStateJson}
          jsonarray={jsonarray}
          setJsonaaray={setJsonaaray}
        />

        <View style={{position: 'absolute', bottom: 20, left: 20, zIndex: 5}}>
          <Button
            rounded
            onPress={() => {
              getMyLocation();
            }}>
            <Icon name="my-location" type="MaterialIcons" />
          </Button>
        </View>

        <Fab
          active={active}
          direction="up"
          containerStyle={{}}
          style={{backgroundColor: '#5067FF', zIndex: 5}}
          position="bottomRight"
          onPress={() => setActive(!active)}>
          <Icon name="plus" type="FontAwesome" />

          <Button
            style={{backgroundColor: '#34A34F', zIndex: 5}}
            onPress={async () => {
              if (type !== null) {
                setType(null);
              } else {
                setType('point');
                const center = await mapRef.current.getCenter();
                const point_feature = turf.point(center);
                setAddPoint([point_feature]);
              }
            }}>
            <Icon name="map-marker" type="FontAwesome" />
          </Button>
          <Button
            style={{backgroundColor: '#3B5998', zIndex: 5}}
            onPress={async () => {
              if (type !== null) {
                setType(null);
              } else {
                setType('line');
                const center = await mapRef.current.getCenter();
                const point_feature = turf.point(center);
                setAddPoint([point_feature]);
              }
            }}>
            <Icon name="flow-line" type="Entypo" />
          </Button>
          <Button
            style={{backgroundColor: '#DD5144', zIndex: 5}}
            onPress={async () => {
              if (type !== null) {
                setType(null);
              } else {
                setType('polygon');
                const center = await mapRef.current.getCenter();
                const point_feature = turf.point(center);
                setAddPoint([point_feature]);
              }
            }}>
            <Icon name="draw-polygon" type="FontAwesome5" />
          </Button>
        </Fab>

        <MapboxGL.MapView
          ref={mapRef}
          onRegionDidChange={onRegionChange}
          onPress={() => {
            if (state !== null) {
              setState(null);
            }
          }}
          style={styles.map}
          styleURL="https://maps.osgeo.in.th/styles/thailand_basemap_1.0/style.json">
          <MapboxGL.ShapeSource id="add_point" shape={geoJson}>
            <MapboxGL.SymbolLayer
              id="marker_point"
              style={{
                iconImage: MarkerImage,
                iconSize: 0.05,
                visibility: type !== null ? 'visible' : 'none',
              }}
            />
          </MapboxGL.ShapeSource>

          <MapboxGL.ShapeSource id="shape_json" shape={geoState}>
            <MapboxGL.CircleLayer
              id="shape_point"
              style={{
                circleColor: '#000000',
                circleRadius: 5,
              }}
              filter={['==', '$type', 'Point']}
            />
            <MapboxGL.LineLayer
              id="shape_line"
              style={{lineColor: '#000000', lineWidth: 5}}
              filter={['==', '$type', 'LineString']}
            />
          </MapboxGL.ShapeSource>

          <CreateLine linecoor={jsonarray.line} pointcoor={jsonarray.point} />

          <MapboxGL.Camera
            zoomLevel={4}
            maxZoomLevel={18}
            centerCoordinate={[100.523186, 13.736717]}
          />
        </MapboxGL.MapView>
      </View>
    </Container>
  );
};

export default MapScreen;
