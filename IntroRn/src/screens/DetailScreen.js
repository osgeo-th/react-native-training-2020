import React from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';

const DetailScreen = () => {
  const {navigate} = useNavigation();
  const {params} = useRoute();
  return (
    <View style={styles.container}>
      <Text>Detail Screen</Text>
      <Button
        title="to Home Screen"
        onPress={() => {
          navigate('Home');
        }}
      />
      <Button
        title="to About Screen"
        onPress={() => {
          navigate('About');
        }}
      />
      <Text>{JSON.stringify(params)}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  constainer: {flex: 1, alignItems: 'center', justifyContent: 'center'},
});

export default DetailScreen;
