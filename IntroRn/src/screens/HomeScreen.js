import React from 'react';
import {View, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Container, Header, Content, Button, Text} from 'native-base';

const HomeScreen = () => {
  const navigation = useNavigation();
  return (
    <Container>
      <Header />
      <Content>
        <Button
          rounded
          light
          onPress={() => {
            alert('Click');
          }}>
          <Text>Click Me!</Text>
        </Button>
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  constainer: {flex: 1, alignItems: 'center', justifyContent: 'center'},
});

export default HomeScreen;
