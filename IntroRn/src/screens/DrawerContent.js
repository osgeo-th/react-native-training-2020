import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';

const ContentDrawer = (props) => {
  return (
    <View style={styles.drawerContent}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View style={styles.headerDrawerSection}>
            <Text
              style={{
                textAlign: 'center',
                fontSize: 18,
              }}>
              Example GIS App
            </Text>
          </View>
          <View style={styles.drawerSection}>
            <DrawerItem
              label="Home"
              onPress={() => {
                props.navigation.navigate('Home');
              }}
            />
            <DrawerItem
              label="Detail"
              onPress={() => {
                props.navigation.navigate('Detail');
              }}
            />
            <DrawerItem
              label="About me"
              onPress={() => {
                props.navigation.navigate('About');
              }}
            />
          </View>
        </View>
      </DrawerContentScrollView>
      <View style={styles.bottomDrawerSection}>
        <DrawerItem
          label="Sign Out"
          onPress={() => {
            // signOut();
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  headerDrawerSection: {
    paddingLeft: 20,
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: '#f4f4f4',
    borderTopWidth: 1,
  },
});

export default ContentDrawer;
