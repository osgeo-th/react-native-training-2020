import React from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const AboutScreen = () => {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <Text>About Screen</Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  constainer: {flex: 1, alignItems: 'center', justifyContent: 'center'},
});

export default AboutScreen;
